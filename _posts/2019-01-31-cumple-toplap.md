---
layout: post
categories: posts
title: Cumple TOPLAP
---

Transmisión 15 aniversario TOPLAP 14-17 de febrero 2019

Primero, un poco de historia, despues instrucciones y opciones para contribuir a la livecodeada online de celebración!

El 14 de febrero de 2003, de noche en un bar lleno de humo de Hamburgo, TOPLAP fue formada por un grupo de artistas y estudiantes que se habian encontrado para explorar la idea emergente del Live Coding (o programacion en vivo/programación al vuelo/programación al tiempo, y diversas maneras de las que lo nombrabamos en aquél entonces). Tuvo lugar despues del, posiblemente, primer encuentro de live coding llamado Changin Grammars (cambiando gramaticas), Llevado adelante por Julian Rohrhuber y Renate Wieser. Tambien estaban presentes Nick Collins, quien vino con el acronimo TOPLAP, y Adrian Ward, quien trajo el logo (que representa personas mirando a la pantalla). Mi memoria es vaga, pero yo (Alex Mclean) también estaba allí - Adrian y yo estabamos mostrando nuestro sistema de Slub, Julian estaba haciendo live coding para peliculas mudas con sus librerias JITLib de SuperCollider, y tambien se nos unía remotamente Ge Wang - no puedo recordar si ocurrió o no. Alberto de Campo también estaba, junto con Reante, Julian y otros miembos de "powerbooks unplugged" en una performance usando Republic network music system en SuperCollider. Quizás Fredrik Olofsson también estaba haciendo visual patching?

Cuando llegué a casa hice la wiki, y el manifiesto que creo que Nick, Adrian y yo empezamos camino a casa (en el aeropuerto de Lubeck - asi que se lo conoce a veces como el manifiesto de Lubeck...) fue editado por muchas personas hasta ser el extraño borrador que tenemos [hoy](https://toplap.org/wiki/ManifestoDraft)

La discusión continuo online y eventualmente mediante el nacimiento de la lista de correo [livecode](https://we.lurk.org/postorius/lists/livecode.we.lurk.org/). Ahora está mucho menos activa (las listas de mail no están tan de moda hoy) pero en los primeros dias fue un centro de discusión extremadamente fértil, que reunió a lxs pocos live coders que estaban diseminados por todo el mundo. [Podes leer el archivo aquí](https://toplap.org/archive/livecode.txt)

De todos modos, esa es suficiente historia... Hemos estado haciendo una transmisión anual de Algorave (fijense las playlist de [eulerroom](https://www.youtube.com/channel/UC_N48pxd05dX53_8vov8zqA/playlists) para mirar los extensos archivos), pero siendo este el 15avo aniversario, creimos que seria divertido convertir este año en un cumpleaños de TOPLAP.

Si queres contribuir - por favor hacelo! queremos el mayor variedad posible de performances, si sos principiante y nunca compartiste tu pantalla anteriormente esta es una excelente manera de empezar - asi que por favor unite en el canal [#toplap15](https://talk.lurk.org/channel/toplap15) del chat de toplap/lurk. Vas a ver el link para anotarte y las instrucciones allí.

Hay alrededor de 150 espacios disponibles desde el 14 al 17 de febrero, al momento de escribir este articulo un tercio estaba ocupado (actualmente 72%), se esta organizando un gran numero de eventos para celebrar alrededor del mundo (Prague, London, NYC, Amsterdam, Madison, Bath, Argentina, Richmond, Hamilton) y otros por individuos que van a livecodear desde su sofa.

Nos vemos ahí y feliz cumpleaños para toda la comunidad de live coding!

Original: [https://toplap.org/toplap-15th-anniversary-stream-14-17th-february-2019/]( https://toplap.org/toplap-15th-anniversary-stream-14-17th-february-2019/)
