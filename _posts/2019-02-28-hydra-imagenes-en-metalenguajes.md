---
layout: post
categories: posts
title: "Hydra: imágenes en metalenguajes"
---

# Hydra: imágenes en metalenguajes

Hydra es un [software libre](https://es.wikipedia.org/wiki/Software_libre) inspirado en los sintetizadores de video analógico modulares. Esta pensado para hacer livecoding de imagenes, visuales y/o videos.Éste nos permite modificar las visuales en tiempo real, a través de la escritura de código, usando el lenguaje de programación JavaScript. A su vez podemos producir formas, señales, diferentes efectos de video (como feedbacks,  desplazamientos, superposiciones de capas, etc). Todo esto es posible desde un navegador web como Chrome o Mozilla. De la misma manera que desde un editor de texto como Atom.

El programa tiene una [documentación en GitHub](https://github.com/ojack/hydra), donde se puede consultar su código fuente y agregar documentación para aportár a la comunidad. Existe también un [canal de chat en Lurk](https://talk.lurk.org/channel/hydra), abierto a todas las personas que quieran consultar y compartir información al respecto.

Hace aproximadamente un año [Olivia Jack](https://twitter.com/_ojack_), su creadore, diseñó este software con la idea de que, como su nombre lo indica, en Hydra haya muchas cabezas pensando y creando. Tomando el concepto de la mitología Greco-Romana, [Hydra de Lerna](https://es.wikipedia.org/wiki/Hidra_de_Lerna) (la bestia de muchas cabezas que por cada cabeza que se corta salen dos más) así como también [Hydra en el reino animal](https://es.wikipedia.org/wiki/Hydra_(animal)) (un ser acuático que posee muchas cabezas y con propiedades regenerativas). Hay* algo en todo esto que se genera y regenera constantemente alrededor del software: la comunidad aporta continuamente los diferentes conocimientos que poseen las personas alrededor del mundo. Convirtiendolo, no solamente en un software para producir imágenes/video, sino en un motor creativo para la producción y reflexión del arte y las maneras de interactuar (simbólica, material y tecnicamente) entre nosotres y con el medio. Todo a partir de la posibilidad que nos brinda internet como medio de comunicación, en torno a eso la comunidad puede contribuir documentando, traduciendo al español para poder tener mayor alcance y comprensión del aprendizaje y el empleo/utilización del programa que al rededor de estos conceptos refuerzan la idea de software libre.

### Más allá del lenguaje, hay un metalenguaje:

Dentro del programa conviven diversos lenguajes, por un lado se encuentra el código escrito, por otro lado la imagen generada por el código, así como también existe el debate y la transmisión de conocimiento y aprendizaje comunitario. Estos días Olivia creó un [bot en Twitter](https://twitter.com/hydra_patterns) capaz de compartir patrones de texturas generadas a partir del programa.Compartiendo además, el código en forma de link para quién quisiese verlo. Permitiendo de esta manera la difusión de lo que se produce y a su vez aportando a una forma de aprendizaje abierto.

Es importante notar la simbiosis que se genera, un ir y venir entre el lenguaje escrito y el lenguaje visual: el código (lenguaje escrito) genera la imagen (lenguaje visual), al mismo tiempo que se pueda ver afectado por él mediante el uso de dispositivos periféricos como web-cams, mouses, uso de tabletas u otras formas de producción visual que se escapan puramente de lo programado, implicando un gesto corporal performático que trasciende a la acción de escribir. Así como también producciones audioritmicas, parametros que respondan al sonido y de esta forma complejizando aún más los sentidos (volviendose un programa que implica no solo la producción visual) y la multiplicidad de lenguajes sucediendo en simultaneo. También pensar que el código es texto, y por tanto maleable a otros niveles de interpretación y usos poéticos más allá de la necesidad técnica, mediante comentarios y un largo etcetera a probar para la apreciación de las participantes.

Hydra constantemente genera y regenera, se dispersa y se propaga.


---

# Hydra, images in creative collaborative metalanguages-thoughts

Hydra is a [free software](https://en.wikipedia.org/wiki/Free_software) inspired in modular analog video synthesizer. It is though for livecoding (live performance) images, pictures and/or videos. It allows the user to generate images in real time through writing and programming of code in JavaScript, producing forms, signals, different video effects such as feedbacks and images displacements, layers superpositions, color modification, scales, etcetera. This all can be done in the browser itself (Chrome, Chromium, -Mozilla-) as well as a text editor like Atom.

Moreover, the program has an up-to-date documentation in [GitHub](https://github.com/ojack/hydra), where one can procur information as well as add when necessary, for the improvement of said documentation. There's also a [channel in Lurk](https://talk.lurk.org/channel/hydra), open to the community to consult and share information in.

Approximately a year ago, [Olivia Jack](https://twitter.com/_ojack_), its creator, designed the software with the idea in mind that, as the name indicates, in Hydra there are many heads thinking and creating. Taking the concept from Greco-Roman mythology, [Lernaean Hydra](https://en.wikipedia.org/wiki/Lernaean_Hydra) (the many-headed beast that with each head cut, two more appear), as well as [Hydra in the animal kingdom](https://en.wikipedia.org/wiki/Hydra_(genus)) (an aqcuatic being that posseses a lot of heads with regenerative propierties), there's something that it's constantly generating and regenerating around the software: the community that continually  contributes from  different knowledges (disciplines) from the people all over the world, making it not only a software to produce images/videos but a creative motor for the production and critical/reflexive thinking of art and the ways we interact (symbolically, materially, technically) with ourselves and the medium. All of this comes from the possibility internet brings as a communication medium/media from where the community can contribute by documenting, translating to spanish (or english ha) to include more people and have better comprehension of the learning and ways to use the programs which reinforce the concept of free software.


## Beyond the language, there's a metalanguage:

Inside the program, diverse lenguages cohabit. On one hand we have the writing of the code, on the other the visual language, related to what's produced on the interface (the image generated by the code) but there is also debate and transmission of knowledge and collective learning also exist. These days, Olivia created a bot on [Twitter](https://twitter.com/hydra_patterns) able to share the patterns of textures generated through the program, also sharing the code as a link for anyone to see, allowing not only the expansion of what's produced but also another way of open learning.

It's important to note the symbiosis between the written language and visual language: the code (written lenguage) generates the visual image (visual language), at the same time that can be affected by the use of peripherical devices like web cams, mouse, tablets or other forms of visual production that scape what's purely programmed, implying a performatic corporal gesture that trascends the act of writing. Also audiorhythms productions, parameters that answer to sound  and in this way intertwining the senses more and the multiplicity of languages happening simultaneously. Besides, code is text, and therefore is malleable to other levels of interpretation and poetics usages beyond the pure technical need, through comments and a long etcetera to be tested by the participants.

Hydra constantly generates and regenerates, it spreads and propagates.
